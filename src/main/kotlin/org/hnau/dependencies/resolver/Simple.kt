package org.hnau.dependencies.resolver

import org.hnau.dependencies.Graph

inline fun <reified T> Graph.simple(noinline resolve: Graph.() -> T) = add(resolve)