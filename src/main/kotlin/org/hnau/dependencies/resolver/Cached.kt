package org.hnau.dependencies.resolver

import org.hnau.dependencies.Graph


class Cached<T>(
        private val resolve: Graph.() -> T
) : (Graph) -> T {

    private var initialized = false
    private var value: T? = null

    @Synchronized
    override fun invoke(
            graph: Graph
    ) = if (initialized) {
        @Suppress("UNCHECKED_CAST")
        value as T
    } else {
        graph.resolve().also { value ->
            this.value = value
            this.initialized = true
        }
    }

}

inline fun <reified T> Graph.cached(noinline resolve: Graph.() -> T) = add(Cached(resolve))