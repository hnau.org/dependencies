package org.hnau.dependencies

import java.util.concurrent.ConcurrentHashMap


class GraphValues {

    private class Item<T>(
            val valueClass: Class<T>,
            val resolver: Graph.() -> T
    )

    private val items = ArrayList<Item<*>>()

    private val cache = ConcurrentHashMap<Class<*>, Graph.() -> Any?>()

    fun <T> set(valueClass: Class<T>, resolver: Graph.() -> T) {
        items.add(Item(valueClass, resolver))
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> get(valueClass: Class<T>): Graph.() -> T {

        fun find(valueClass: Class<*>): Graph.() -> Any? {
            items.forEach { item ->
                if (valueClass.isAssignableFrom(item.valueClass)) {
                    return item.resolver
                }
            }
            throw IllegalStateException("Value for class ${valueClass.name} not found")
        }

        var result = cache[valueClass]
        if (result == null) {
            result = find(valueClass)
            cache[valueClass] = result
        }
        return result as Graph.() -> T
    }

}