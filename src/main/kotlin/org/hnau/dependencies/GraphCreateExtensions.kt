package org.hnau.dependencies


inline fun Graph.Companion.create(create: Graph.() -> Unit) = Graph().apply(create)