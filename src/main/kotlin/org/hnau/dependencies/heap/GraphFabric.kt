package org.hnau.dependencies.heap

import org.hnau.dependencies.Graph


interface GraphFabric {

    companion object {

        fun create(
                createGraph: () -> Graph
        ) = object: GraphFabric {
            override fun createGraph() = createGraph.invoke()
        }

    }

    fun createGraph(): Graph

}