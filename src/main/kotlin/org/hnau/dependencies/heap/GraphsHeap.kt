package org.hnau.dependencies.heap

import org.hnau.dependencies.Graph


class GraphsHeap {

    private val heap = HashMap<GraphFabric, Graph>()

    operator fun get(graphFabric: GraphFabric): Graph {
        var result = heap[graphFabric]
        if (result == null) {
            result = graphFabric.createGraph()
            heap[graphFabric] = result
        }
        return result
    }

    fun release(graphFabric: GraphFabric) {
        heap.remove(graphFabric)
    }

}