package org.hnau.dependencies


class Graph {

    companion object {}

    private val values = GraphValues()

    inline fun <reified T> add(noinline resolve: Graph.() -> T) =
            add(T::class.java, resolve)

    fun <T> add(valueClass: Class<T>, resolve: Graph.() -> T) {
        values.set(valueClass, resolve)
    }

    inline fun <reified T> get() =
            get(T::class.java)

    @Suppress("UNCHECKED_CAST")
    fun <T> get(valueClass: Class<T>) =
            values.get(valueClass).invoke(this)

}